\documentclass[11pt]{beamer}  %% versione proiettore
%%\documentclass[11pt,handout]{beamer} %% versione stampa
\usepackage{lucidiJb-2ed}

\usepackage{relsize}

\mode<article>
{
  \usepackage{fullpage}
  \usepackage{hyperref}
}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{Ethereum}
  \usefonttheme[onlysmall]{structurebold}
}

\subtitle{Learning Ethereum}
\title{Smart Contracts}
\institute{Universit\`a di Verona, Italy}
\date{January 2020}

\setbeamercovered{invisible}

\def\codesize{\smaller}
\def\<#1>{\codeid{#1}}
\newcommand{\codeid}[1]{\ifmmode{\mbox{\codesize\ttfamily{#1}}}\else{\codesize\ttfamily #1}\fi}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}\frametitle{A simple Ponzi scheme}

  \begin{center}
    \includegraphics[width=\textwidth,clip=false]{pictures/simple-ponzi.png}
  \end{center}

  The first investment will be burned to address \<0x0>.
  
\end{frame}

\begin{frame}\frametitle{Exercise}
  \begin{enumerate}
  \item create an account with MetaMask
  \item charge it from the Ropsten faucet with 1 ETH
  \item write the \<SimplePonzi.sol> contract in Remix
  \item compile the contract in Remix
  \item deploy the contract in Ropsten with Remix
  \item connect to the contract in Ropsten by somebody else
  \item start calling the fallback function with increasing value
    (with Remix of with MetaMask; for the latter, remember to increase
    to gas limit for sending ETH)
  \end{enumerate}

  \bigskip
  You can check the current investment and investor from Remix.

\end{frame}

\begin{frame}\frametitle{A gradual Ponzi scheme}

  \begin{center}
    \includegraphics[scale=0.4,clip=false]{pictures/gradual-ponzi.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A simple lottery (1)}

  \begin{center}
    \includegraphics[scale=0.4,clip=false]{pictures/simple-lottery-1.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A simple lottery (2)}

  \begin{center}
    \includegraphics[scale=0.45,clip=false]{pictures/simple-lottery-2.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A simple lottery (3)}

  \begin{center}
    \includegraphics[scale=0.45,clip=false]{pictures/simple-lottery-3.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A simple lottery (4)}

  \begin{center}
    \includegraphics[scale=0.45,clip=false]{pictures/simple-lottery-4.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Exercise}
  \begin{enumerate}
  \item create an account with MetaMask
  \item charge it from the Ropsten faucet with 1 ETH
  \item write the \<SimpleLottery.sol> contract in Remix
  \item compile the contract in Remix
  \item deploy the contract in Ropsten with Remix, with 3 minutes for buying tickets
  \item connect to the contract in Ropsten by somebody else
  \item buy some tickets before the 3 minutes expire
    (with Remix or with MetaMask; for the latter, remember to increase
    to gas limit for sending ETH)
  \item the owner of the lottery contract draws the winner
  \item try to withdraw your price (if any!)
  \end{enumerate}

  \medskip
  You can check the winner from Remix.

\end{frame}

\begin{frame}\frametitle{A simple auction (1)}

  \begin{center}
    \includegraphics[scale=0.55,clip=false]{pictures/simple-auction-1.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A simple auction (2)}

  \begin{center}
    \includegraphics[scale=0.48,clip=false]{pictures/simple-auction-2.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A simple auction (3)}

  \begin{center}
    \includegraphics[scale=0.45,clip=false]{pictures/simple-auction-3.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A simple auction (4)}

  \begin{center}
    \includegraphics[scale=0.52,clip=false]{pictures/simple-auction-4.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Why this complicated withdraw pattern?}

  \begin{center}
    \includegraphics[scale=0.5,clip=false]{pictures/simple-auction-withdraw.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Why this complicated withdraw pattern?}

  \begin{greenbox}{\<highestBidder.transfer(highestBid); DON'T DO THIS!!!>}
    The problem with this call (inside function \<bid()>) is that
    a broken or malicious bidder might redefine its own fallback function in
    such a way to consume all gas (for instance):
    \begin{itemize}
    \item all subsequent bidders will get a failure when calling \<bid()>
    \item the broken or malicious contract will never be replaced by another bidder
    \end{itemize}
  \end{greenbox}

\end{frame}

\begin{frame}\frametitle{The reentrancy nightmare}

  \only<1>{
  \begin{center}
    \includegraphics[scale=0.50,clip=false]{pictures/simple-auction-withdraw-only.png}
  \end{center}
  }
  \only<2>{
    \begin{center}
      Beware: code vulnerable to reentrancy:\\
    \includegraphics[scale=0.50,clip=false]{pictures/simple-auction-withdraw-only-wrong.png}
  \end{center}
  }

  \only<1>{
    Why this complication of setting \<pendingReturns[msg.sender]=0>
    so early?
  }
    
  \only<2>{
  \begin{greenbox}{Order does matter}
    \begin{itemize}
    \item a malicious bidder might redefine its own fallback function
      in order to call \<withdraw()> back again, as many times as it likes
    \item it will withdraw its \<amount> as many times as it wants, until
      the auction contract is depleted
    \item basically, it runs away with the highest bid and with the bids of all bidders
      that have not been withdrawn yet
    \end{itemize}
  \end{greenbox}
  }

\end{frame}

\begin{frame}\frametitle{The reentrancy nightmare}

  \begin{center}
    \includegraphics[width=\textwidth,clip=false]{pictures/re-entrancy.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{The DAO attack (2016)}

  \begin{greenbox}{The most famous reentrancy exploit}
    \begin{itemize}
    \item the DAO was a contract for autonomous decentralized organizations
    \item the attacker used reentrancy to steal $50\text{M}\$$ equivalent of ETH
    \item the Ethereum team decided to change the consensus rules in order
      to make such transactions illegal and get some of that money back
    \item some node maintainers didn't accept the change and continued
      operating with the old rules, leading to a network fork known as
      Ethereum Classic
    \end{itemize}
  \end{greenbox}

  \begin{center}
    \includegraphics[scale=0.5,clip=false]{pictures/ethereum-vs-ethereum-classic.jpg}
  \end{center}

\end{frame}

\begin{frame}[fragile]\frametitle{Are \<send()> and \<transfer()> safe from reentrancy?}

  \begin{greenbox}{Currently yes}
    \begin{itemize}
    \item they only forward $2300$ units of gas, not enough for reentrancy
    \item differently from the low-level \verb!msg.sender.call.value(amount)("")!, that forwards all gas and definitely allows reentrancy
    \item however:
      \begin{itemize}
      \item gas costs might change in the future, allowing reentrancy also
        with $2300$ units of gas
      \item the programmer might decide to forward more gas explicitly in order to deal
        with contracts with complex fallback functions
      \item \<send()> and \<transfer()> risk to be deprecated soon\ldots
      \end{itemize}
    \item in conclusion, it's good practice to
      set \<pendingReturns[msg.sender] = 0>
      before sending the money to
      \<msg.sender>, however that last operation is performed
    \end{itemize}
  \end{greenbox}

\end{frame}

\begin{frame}\frametitle{A blind auction (1)}

  \begin{greenbox}{In our simple auction, everything happens in clear}
    We want a \alert{blind auction} now, where:
    \begin{description}
    \item[start] an EOA starts the auction and becomes its owner
    \item[bidding phase] bidders place zero, one or more bids in \alert{closed envelopes};
      some envelopes might contain bids marked as \alert{fake}, which allows one to bluff
    \item[revealing phase] bidders provide the secret needed to open the envelopes
    \item[end] the best non-fake bid is forwarded to the auction's owner
    \end{description}
  \end{greenbox}

  \begin{center}
    \includegraphics[scale=0.1,clip=false]{pictures/blind.jpg}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A blind auction (2)}

  \begin{center}
    \includegraphics[scale=0.5,clip=false]{pictures/blind-auction-1.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A blind auction (3)}

  \begin{center}
    \includegraphics[scale=0.42,clip=false]{pictures/blind-auction-2.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A blind auction (4)}

  \begin{center}
    \includegraphics[scale=0.35,clip=false]{pictures/blind-auction-3.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{A blind auction (5)}

  \begin{center}
    \includegraphics[scale=0.5,clip=false]{pictures/blind-auction-4.png}
  \end{center}

\end{frame}

\begin{frame}\frametitle{Exercise}

  \begin{greenbox}{Write a \<TicTacToe.sol> smart contract}
    \begin{itemize}
    \item the first EOA that calls the payable function \<play(x,y)> becomes player~1:
      she must pay at least $0.01$ ETH
    \item the second EOA that calls the payable function \<play(x,y)> becomes player~2:
      he must be different from player~1;
      he must pay at least what player~1 payed
    \item subsequent calls to \<play> do not require any payed amount
    \item if one player wins, the balance of the game goes
      \begin{itemize}
      \item $90\%$ to the winner
      \item $10\%$ to the creator of the game
      \end{itemize}
    \item if the game is a draw, $10\%$ of the balance of the game goes to the creator of the game
    \item after victory or draw, the game resets and is ready to accept new players
    \end{itemize}
  \end{greenbox}

\end{frame}

\begin{frame}\frametitle{Exercise}

  \begin{greenbox}{Write a user interface that connects to the \<TicTacToe.sol> smart contract}
    \begin{itemize}
    \item as a Java textual desktop application using Infura
    \item as a Java graphical desktop application using Infura
    \item as an Android mobile application using Infura
    \item as a web application (Javascript) using Infura
    \item the same as above, but connecting directly to an Ethereum node\ldots
    \end{itemize}
  \end{greenbox}

\end{frame}

\end{document}
